``` bash
export $(cat ./local.env | xargs) && docker-compose -f ./deployment/docker-compose.yml up --force-recreate --build erp gateway
```
