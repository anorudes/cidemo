ARG GOLANG_SDK_VERSION

## stage 1: builder image
FROM golang:${GOLANG_SDK_VERSION}-alpine3.13 AS builder

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
ARG CI_DEPLOY_USER
ARG CI_DEPLOY_PASSWORD

WORKDIR /build

# install common dependencies
RUN apk update && apk add --no-cache \
	make

# allow pulling private modules
RUN echo "machine gitlab.com login ${CI_DEPLOY_USER} password ${CI_DEPLOY_PASSWORD}" > ~/.netrc

# download dependencies
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# copy and build the app and tests
COPY ./ ./
RUN go build -o ./app ./...

## stage 2: tester image
FROM builder AS tester
CMD make test-with-coverage

## stage 3: runtime image
FROM alpine:latest AS runner
COPY --from=builder /build .
CMD ./app
