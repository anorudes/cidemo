package main

import (
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// echo instance
	e := echo.New()

	// add middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// register handler in router
	e.GET("/lol", lolHandler)

	// start server and wait stop signal
	e.Logger.Fatal(e.Start(os.ExpandEnv("${ERP_SERVER_HOST}:${ERP_SERVER_PORT}")))
}

func lolHandler(c echo.Context) error {
	resp := echo.Map{
		"kek": "cheburek",
		"now": time.Now(),
		"whoami": os.ExpandEnv("i am ${GITLAB_USER_LOGIN}"),
	}
	return c.JSON(http.StatusOK, resp)
}
