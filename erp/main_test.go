package main

import (
	"fmt"
	"testing"
	"time"
)

func TestErp_Positive(t *testing.T) {
	fmt.Println("start test")
	v := true
	if !v {
		t.Errorf("erp test failed")
	}
	time.Sleep(5*time.Second)
	fmt.Println("finish test")
}

func TestErp_Negative(t *testing.T) {
	t.SkipNow()
	fmt.Println("start test")
	time.Sleep(5*time.Second)
	t.Errorf("erp test failed")
}
