test-with-coverage:
	mkdir -p ./artifacts
	chmod -R 777 ./artifacts # make it possible to delete at volumes level
	go test -v ./... -coverprofile=./artifacts/coverage.out
	go tool cover -html=./artifacts/coverage.out -o ./artifacts/coverage.html
